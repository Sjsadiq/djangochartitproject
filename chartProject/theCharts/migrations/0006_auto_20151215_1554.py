# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('theCharts', '0005_auto_20151215_1533'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='ChartdemoMonthlyweatherbycity',
            new_name='chartdemo_monthlyweatherbycity',
        ),
    ]
