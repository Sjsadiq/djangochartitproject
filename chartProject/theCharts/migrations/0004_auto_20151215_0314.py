# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('theCharts', '0003_auto_20151215_0313'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='chartdemo_monthlyweatherbycity',
            new_name='MonthlyWeatherByCity',
        ),
    ]
