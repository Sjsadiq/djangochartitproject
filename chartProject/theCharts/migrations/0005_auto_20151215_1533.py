# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('theCharts', '0004_auto_20151215_0314'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='MonthlyWeatherByCity',
            new_name='ChartdemoMonthlyweatherbycity',
        ),
    ]
