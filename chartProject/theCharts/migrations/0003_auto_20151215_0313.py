# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('theCharts', '0002_dailyweather'),
    ]

    operations = [
        migrations.CreateModel(
            name='chartdemo_monthlyweatherbycity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('month', models.IntegerField()),
                ('boston_temp', models.DecimalField(max_digits=5, decimal_places=1)),
                ('houston_temp', models.DecimalField(max_digits=5, decimal_places=1)),
            ],
        ),
        migrations.DeleteModel(
            name='MonthlyWeatherByCity',
        ),
    ]
