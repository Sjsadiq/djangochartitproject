# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('theCharts', '0006_auto_20151215_1554'),
    ]

    operations = [
        migrations.DeleteModel(
            name='chartdemo_monthlyweatherbycity',
        ),
        migrations.DeleteModel(
            name='DailyWeather',
        ),
    ]
